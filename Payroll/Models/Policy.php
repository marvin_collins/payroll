<?php

namespace Payroll\Models;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    const MODULE_ID = 30;

    protected $fillable = ['module_id', 'policy', 'value', 'exceptions'];

    const PERMISSIONS = [
        'Create'    => 'policy.create',
        'Read'      => 'policy.read',
        'Update'    => 'policy.update',
        'Delete'    => 'policy.delete'
    ];

    public function scopeEnabled($query)
    {
        return $query->whereEnabled(true);
    }

    public function scopeDisabled($query)
    {
        return $query->whereEnabled(false);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
