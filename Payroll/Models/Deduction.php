<?php

namespace Payroll\Models;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    protected $guarded = [];

    const MODULE_ID = 8;

    const PERMISSIONS = [
        'Create'    => 'deduction.create',
        'Read'      => 'deduction.read',
        'Update'    => 'deduction.update',
        'Delete'    => 'deduction.delete'
    ];

    const
        PAYE = 1,
        NHIF = 2,
        NSSF = 3;

    public function relief()
    {
        return $this->morphOne(Relief::class, 'reliefable');
    }

    public function slabs()
    {
        return $this->hasMany(DeductionSlab::class);
    }

    public function employeeDeductions()
    {
        return $this->hasMany(EmployeeDeduction::class);
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_deductions');
    }

    public function payments()
    {
        return $this->hasMany(DeductionPayments::class);
    }

    public function __distruct()
    {
        $this->employees = null;
    }
}
