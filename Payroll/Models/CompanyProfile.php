<?php

namespace Payroll\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $guarded = [];

    const MODULE_ID = 6;

    const PERMISSIONS = [
        'Create'    => 'company.create',
        'Read'      => 'company.read',
        'Update'    => 'company.update',
        'Delete'    => 'company.delete'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
