<?php

namespace Payroll\Models;

use Illuminate\Database\Eloquent\Model;

class DeductionSlab extends Model
{
    protected $guarded = [];

    const MODULE_ID = 10;

    public function deduction()
    {
        return $this->belongsTo(Deduction::class);
    }
}
