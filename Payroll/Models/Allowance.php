<?php

namespace Payroll\Models;

use Illuminate\Database\Eloquent\Model;

class Allowance extends Model
{
    protected $guarded = [];

    const MODULE_ID = 3;

    const PERMISSIONS = [
        'Create'    => 'allowance.create',
        'Read'      => 'allowance.read',
        'Update'    => 'allowance.update',
        'Delete'    => 'allowance.delete'
    ];

    public function relief()
    {
        return $this->morphOne(Relief::class, 'reliefable');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function employeeAllowances()
    {
        return $this->hasMany(EmployeeAllowance::class);
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_allowances');
    }
}
