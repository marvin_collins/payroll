<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Payroll\Models\Allowance;
use Payroll\Models\CompanyProfile;
use Payroll\Models\Deduction;
use Payroll\Models\Payroll;
use Payroll\Models\Policy;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layout'], function ($view) {
            $company = CompanyProfile::first();
            $daysPolicy = Policy::whereModuleId(Payroll::MODULE_ID)
                ->wherePolicy(Payroll::ENABLE_DAYS_ATTENDANCE)
                ->first()->value;

            $view->withCompany($company)
                ->withDaysEnabled($daysPolicy);
        });

        Relation::morphMap([
            'Allowance' => Allowance::class,
            'Deduction' => Deduction::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
