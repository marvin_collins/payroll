<?php

namespace App\Http\Controllers;

use App\Overtime;
use App\OvertimeAward;
use App\OvertimeSlab;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Payroll\Models\EmployeeContract;
use Payroll\Models\Holiday;
use Payroll\Models\Policy;
use Payroll\Parsers\ModelFilter;
use Payroll\Models\Employee;
use Payroll\Parsers\BulkAssigner;
use Payroll\Factories\HTMLElementsFactory;
use Illuminate\Support\Str;

class OvertimeAwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const FIELD_NAME = 'time_hours';

    public function index()
    {
        $overtimes = OvertimeAward::orderBy('ot_date','desc')->get()->unique('ot_date');
//        dd($overtimes);
        return view('modules.payroll.overtimeaward.index',compact('overtimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BulkAssigner $assigner)
    {

        $requiredFields [] = [
            'name' => 'time_hours',
            'type' => HTMLElementsFactory::TEXT
        ];


//        //$holidays = Holiday::all();
    $allemployees = collect(Employee::all());
        $rows = $allemployees->each(function ($item, $key) use ($allemployees) {
            $allemployees[$key] = collect($item)->only([
                'id', 'payroll_number', 'first_name', 'last_name', 'identification_number'
            ]);
        })->toArray();

$columns = $allemployees->each(function ($item, $key) use ($allemployees) {
    $allemployees[$key] = collect($item)->only([
        'payroll_number', 'first_name', 'last_name', 'identification_number'
    ]);
})->first()->keys()->toArray();
//    dd($columns);
       // dd($requiredFields);

       return view('modules.payroll.overtimeaward.create')
           ->with('rows',$rows)
           ->with('columns',$columns)
           ->with('requiredFields',$requiredFields)
           ->with('formAction',route('overtimeaward.index'))
           ->with('allowCopy',$allowCopy = false)
           ->with('assignToId',Carbon::now());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {


        $overtimeCollect = collect(request()->all());
        $filterData = $overtimeCollect->filter(function($value, $key){
            return Str::startsWith($key, 'time_hours') && $value != '';
        });

        foreach ($filterData as $key => $value) {
            $insert [] = [
                'employee_id' => substr($key, strlen('time_hours')),
                'ot' => $value,
                'ot_cash' =>$this->getOtcash($overtimeCollect['ot_date'],$value,substr($key, strlen('time_hours'))),
                'ot_date' => $overtimeCollect['ot_date'],
                'ot_status' => 'Unpaid',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
//        dd($insert);
        OvertimeAward::insert($insert);
    }

    public function getOtcash($dateSelected,$hours,$employee)
    {
        $getHoliday = Holiday::all()->toArray();

        $arrayValue = count($getHoliday);
        if (($arrayValue == 0) && $dateSelected != Carbon::parse($dateSelected)->isWeekend()) {
            //this is standard

            return ($this->calculateOT($hours,$rate='rate',$employee));
        }

        elseif(($arrayValue == 0) && $dateSelected == Carbon::parse($dateSelected)->isWeekend()) {
            //special
            return ($this->calculateOT($hours,$rate='slab',$employee));


        }
        else{
            foreach ($getHoliday as $holiday) {
                $date = $holiday['holiday_day'] . '-' . $holiday['holiday_month'] . '-' . Carbon::now()->year;
                if ($dateSelected != $date && $dateSelected != Carbon::parse($dateSelected)->isWeekend()) {
                    return ($this->calculateOT($hours,$rate='slab',$employee));
                }
//                dd('got you');
            }
        }

    }

    public function calculateOT($hours,$rate,$employee)
    {//find employee
        $salary = EmployeeContract::where('employee_id',$employee)->get();
        $theEmployeeSalary = $salary->each(function ($item, $key) use ($salary){
            $salary[$key] = collect($item)->only(['current_basic_salary']);
        })->first();
        //company days
        $numberDays= Policy::where('module_id',29)->get();
        $days = $numberDays->each(function ($item, $key) use ($numberDays) {
            $numberDays[$key] = collect($item)->only([
                'value'
            ]);
        })->last();
        //slary per hr
        $employSalary = ($theEmployeeSalary['current_basic_salary']/ $days['value']);
//overtime type
        $otType = Overtime::where('type',$rate)->get();
        $rowrate = $otType->each(function ($item, $key) use ($otType) {
            $otType[$key] = collect($item)->only([
                'id','type', 'rate'
            ]);
        })->toArray();
        foreach($rowrate as $rate){
            if($rate['type'] == 'rate'){
                $money = $this->calculateRate($hours,$rate['rate'],$employSalary);

                return $money;
            }

            else {
                $rows = OvertimeSlab::all()->where('overtime_id', $rate['id'])->toArray();
                $i = count($rows) + 1;
                return $this->calculateSlab($hours,$employSalary,$rate['id'],$i,$rows,$total = 0);
            }
        }

    }

    public function calculateRate($hours, $sendRate,$employSalary)
    {

        return (number_format(($hours*$sendRate*$employSalary),2));
    }

    public function calculateSlab($hours,$employSalary,$id,$i,$rows,$total)
    {$otslabs = collect($rows)->each(function($item, $key) use($rows){
        $rows[$key] = collect($item)->only(['min_time','max_time','rate']);
    })->toArray();
        foreach($otslabs as $otslab){
            $time = ($otslab['max_time']-$otslab['min_time'])+1;
            if(is_array($otslab) && ($hours > $time && $hours != $time)){
                $hours = $hours - $time;
                $total = $total + ($employSalary*$time * $otslab['rate']);

            }
            else {
                $total = $total + ($employSalary * $hours * $otslab['rate']);
//                echo $total . ' ' . $hours . $otslab['rate'] ;
                return $total;
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (count(explode('-', $id)) > 1) {
            $overtimeawardsarray = OvertimeAward::where('ot_date', '<=', Carbon::parse('1-'. $id)->endOfMonth()->format('d-m-Y'))
                ->get()->toArray();
            foreach($overtimeawardsarray as $key => $ota){
                $overtimeawards [] = [
                    'id' =>$ota['id'],
                    'employee_id' => $ota['employee_id'],
                    'first_name' => $this->getEmployee($ota['employee_id'],$rq = 'first_name'),
                    'last_name' => $this->getEmployee($ota['employee_id'],$rq = 'last_name'),
                    'payroll' => $this->getEmployee($ota['employee_id'],$rq = 'payroll'),
                    'ot_cash' => $ota['ot_cash'],
                    'ot_status' => $ota['ot_status'],
                    'ot_date' => $ota['ot_date']

                ];
            }
//dd($overtimeawardsarray);
            return view('modules.payroll.overtimeaward.show')
                ->withTitle(Carbon::parse('30-' . $id)->format('F Y'))
                ->withOvertimeawards($overtimeawards);
        }

        $advances = $advance
            ->where('for_month', '>=', Carbon::parse('01-01-' . $id)->format('Y-m-d'))
            ->where('for_month', '<=', Carbon::parse('31-12-' . $id)->format('Y-m-d'))
            ->get();

        return view('modules.payroll.advances.show')
            ->withTitle(Carbon::parse('01-01-' . $id)->format('Y'))
            ->withAdvances($advances);
    }

    public function getEmployee($id,$requetdata)
    {
        $employee = Employee::select('payroll_number','first_name','last_name')->where('id',$id)->get()->toArray();
        foreach($employee as $name)
        {
            $firstname = $name['first_name'];
            $lastname = $name['last_name'];
            $payroll = $name['payroll_number'];
            if($requetdata == 'first_name')
            {
                return $firstname;
            }if($requetdata == 'last_name')
            {
                return $lastname;
            }else
            {
                return $payroll;
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
