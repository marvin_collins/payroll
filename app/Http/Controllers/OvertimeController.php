<?php

namespace App\Http\Controllers;

use App\Overtime;
use App\OvertimeSlab;
use Illuminate\Http\Request;

use App\Http\Requests;

class OvertimeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $overtimes = Overtime::all()->sortByDesc('id');
        return view('modules.company.overtime.index',compact('overtimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.company.overtime.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Overtime::create($request->all());
        return redirect()->route('overtime.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        return view('modules.company.overtime.show', compact('overtimeShow'));
        return $this->overtimeData($id,'modules.company.overtime.show');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->overtimeData($id,'modules.company.overtime.edit');
    }


    public function overtimeData($id, $view)
    {
        $overtime = Overtime::findorfail($id);
        $slabs = OvertimeSlab::where('overtime_id',$overtime->id);
        if($overtime->type == 'slab'){
            $slabs = OvertimeSlab::where('overtime_id',$overtime->id)->orderBy('slab_number','ASC')->get();
        }

        return view($view)->withOvertime($overtime)->withSlabs($slabs);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $overtime =Overtime::findorfail($id);
        $slabs = OvertimeSlab::where('overtime_id',$id);
        $oldOvertime = $overtime -> replicate();
        $newData = $request->only(['name','type','rate']);
   if($newData['type'] == 'rate' && $oldOvertime['type'] == 'slab'){
       $slabs->delete();
       $overtime->fill($newData)->save();
       return redirect()->route('overtime.index');
   }
        else{
            $slabs->delete();
            $overtime->delete();
            $data = $request->all() ;
            $data['rate']=null;
            Overtime::create($data);
        return redirect()->route('overtime.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $overtime = Overtime::findorfail($id);
        if($overtime['type'] == 'slab')
        OvertimeSlab::where('overtime_id',$overtime['id'])->delete();
        $overtime->delete();
        return redirect()->route('overtime.index');

    }
}
