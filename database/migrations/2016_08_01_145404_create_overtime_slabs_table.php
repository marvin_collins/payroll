<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimeSlabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtime_slabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('overtime_id')->unsigned();
            $table->integer('slab_number');
            $table->double('min_time');
            $table->double('max_time');
            $table->string('rate');
            $table->timestamps();

            $table->foreign('overtime_id')
                ->references('id')
                ->on('overtimes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('overtime_slabs');
    }
}
