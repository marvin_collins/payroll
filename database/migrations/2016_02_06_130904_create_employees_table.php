<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payroll_number'); // instead of using employee_id we
            // use payroll number to prevent duplicate keys on relationships
            $table->string('avatar');
            $table->string('identification_number');
            $table->enum('identification_type', ['National ID', 'Passport']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->string('nationality')->nullable();
            $table->date('date_of_birth');
            $table->enum('gender', ['Male', 'Female']);
            $table->string('email');
            $table->string('mobile_phone', 13);
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->date('join_date');
            $table->integer('payment_structure_id')->unsigned();
            $table->timestamps();
            $table->softDeletes(); // works also as termination date
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
