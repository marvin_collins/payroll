<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeductionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deduction_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('deduction_id')->unsigned()->index();
            $table->bigInteger('employee_id')->unsigned()->index();
            $table->string('deduction_number');
            $table->double('amount');
            $table->date('for_month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deduction_payments');
    }
}
