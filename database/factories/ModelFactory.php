
<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Overtime::class, function(Faker\Generator $overtime){
    $array = ['rate','slab'];
    $array2 = ['SPE','STD'];
    $typp = array_rand($array,1);
    $overt = array_rand($array2,1);
    return [
        'name' =>$array2[$overt],
        'type' =>$array[$typp],
        'created_at'=>\Carbon\Carbon::now(),
        'updated_at'=>\Carbon\Carbon::now(),

        ];
});
