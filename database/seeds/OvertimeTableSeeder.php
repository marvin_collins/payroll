<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OvertimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();
        Db::table('overtimes')->insert([
        ['name' => 'STANDARD OT',
            'type' => 'rate',
            'rate' => '1.5',
            'created_at' => $now,
            'updated_at' => $now],

        ['name' => 'SPECIAL OT',
            'type' => 'rate',
            'rate' => '2',
            'created_at' => $now,
            'updated_at' => $now        ]
    ]);

    }
}
