@extends('layout')
@section('content')
    <div class="page-head">
        <div class="page-title">
            <h1>Employees - <small> View the currently employed persons</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('employees.index') }}">Employee</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Create</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-sm-12">
            <form action="{{ route('employees.update', $input->id) }}" method="post" role="form">
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="fa fa-briefcase font-red-sunglo"></i>
                                    <span class="caption-subject bsession uppercase"> Employee Details</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" id="payroll_number" name="payroll_number" value="{{ $input->payroll_number }}" required>
                                        <label for="payroll_number">Payroll Number* (e.g. WI02314/2013)</label>
                                        <span class="help-block">This is the payroll number that will be used to refer to the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $input->first_name }}" required>
                                        <label for="first_name">First Name*</label>
                                        <span class="help-block">This is the first name of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" id="middle_name" name="middle_name" value="{{ $input->middle_name }}">
                                        <label for="middle_name">Middle Name</label>
                                        <span class="help-block">This is the middle name of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $input->last_name }}" required>
                                        <label for="last_name">Last Name*</label>
                                        <span class="help-block">This is the last name of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" id="identification_type" name="identification_type">
                                            <option value="National ID" {{ $input->identification_type == "National ID" ? 'selected' : '' }}>National ID</option>
                                            <option value="Passport" {{ $input->identification_type == "Passport" ? 'selected' : '' }}>Passport</option>
                                        </select>
                                        <label for="identification_type">Identification Type*</label>
                                        <span class="help-block">This is the type of identification to be used by the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" id="identification_number" name="identification_number" value="{{ $input->identification_number }}" required>
                                        <label for="identification_number">Identification Number*</label>
                                        <span class="help-block">This is either the passport number or the national ID of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="Female" {{ ('Female' or $input->gender) == "Female" ? 'selected' : '' }}>Female</option>
                                            <option value="Male" {{ $input->gender == "Male" ? 'selected' : '' }}>Male</option>
                                        </select>
                                        <label for="gender">Gender*</label>
                                        <span class="help-block">This is the gender of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="submit" class="btn btn-primary" value="Save">
                                        <a class="btn btn-danger" href="{{ URL::previous() }}">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="fa fa-briefcase font-red-sunglo"></i>
                                    <span class="caption-subject bsession uppercase"> Other Details</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <label for="date_of_birth">Date of Birth*</label>
                                        <div class="input-group date date-picker margin-bottom-5" readonly data-date-format="dd-mm-yyyy">
                                            <input type="text" class="form-control form-filter input-sm" value="{{ $input->date_of_birth }}" name="date_of_birth" required>
											<span class="input-group-btn">											    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
                                        </div>
                                        <span>This is the date of birth of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" id="mobile_phone" name="mobile_phone" class="form-control" value="{{ $input->mobile_phone }}" required>
                                        <label for="mobile_phone">Mobile Phone*</label>
                                        <span class="help-block">This is the mobile phone number of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="email" value="{{ $input->email }}" id="email" name="email" class="form-control" required>
                                        <label for="email">Email*</label>
                                        <span class="help-block">This is the personal email of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" id="address" value="{{ $input->address }}" name="address" class="form-control">
                                        <label for="address">Address</label>
                                        <span class="help-block">This is the postal address of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" id="postal_code" name="postal_code" value="{{ $input->postal_code }}" class="form-control">
                                        <label for="postal_code">Postal Code</label>
                                        <span class="help-block">This is the postal code of the employee</span>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <?php
                                        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Msessionova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
                                        ?>
                                        <select class="form-control" id="nationality" name="nationality">
                                            @foreach($countries as $country)
                                                <option value="{{ $country }}" {{ "Kenya" == $country ? 'selected' : '' }}>{{ $country }}</option>
                                            @endforeach
                                        </select>
                                        <label for="nationality">Nationality*</label>
                                        <span class="help-block">This is the nationality the employee</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

