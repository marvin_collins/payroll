@extends('layout')

@section('content')
    <div class="page-head">
        <div class="page-title">
            <h1>Overtime</h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Overtime</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font-color hide"></i>
                        <span class="caption-subject theme-font-color bold uppercase">Current overtime rate</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-hover table-responsive dataTable" id="overtimes_table">
                        <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Rate
                            </th>
                            <th>
                                Edit
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($overtimes as $overtime)
                            <tr>
                                <td>
                                    <a href="{{ route('overtime.show', ['id' => $overtime->id]) }}">{{ $overtime->name }}</a>
                                </td>
                                <td class="text-center">
                                    {{ title_case(str_replace('_', ' ', $overtime->type)) }}
                                </td>
                                <td class="text-center">
                                    {!! $overtime->type == 'slab' ? '<a href="' . route('overtime.show', ['id' => $overtime->id]) . '">View Slab</a>' : number_format($overtime->rate, 2) !!}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-success btn-xs" href="{{ route('overtime.edit', ['overtime' => $overtime->id]) }}">
                                        Edit </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>

@endsection

