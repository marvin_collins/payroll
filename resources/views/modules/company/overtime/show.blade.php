@extends('layout')
@section('content')
    <div class="page-head">
        <div class="page-title">
            <h1>Overtime - <small> </small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('overtime.index') }}">Overtime</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">View</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-sm-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>{{ $overtime->name }} Details
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-custom nav-justified">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#details" data-toggle="tab">
                                    Overtime Details </a>
                            </li>
                            <li>
                                <a href="#payments" data-toggle="tab">
                                    Overtime Payments </a>
                            </li>
                            <li>
                                <a href="#employees" data-toggle="tab">
                                    Employees with {{ $overtime->name }} </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="details">
                                <div class="form-body row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <label for="name">Overtime Name</label>
                                            <div class="form-control">{{ $overtime->name }}</div>
                                        </div>
                                       <!-- <div class="form-group form-md-line-input">
                                            <label for="name">Exemption</label>
                                            <div class="form-control">Below </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label for="name">Has Relief?</label>
                                            <div class="form-control"></div>
                                        </div>
-->

                                        <div class="form-group form-md-line-input">
                                            <label for="name">Overtime Type</label>
                                            <div class="form-control">{{ title_case(str_replace('_', ' ', $overtime->type)) }}</div>
                                        </div>
                                        @if($overtime->type == 'rate')
                                            <div class="form-group form-md-line-input">
                                                <label for="name">Rate</label>
                                                <div class="form-control">{{ number_format($overtime->rate, 2) }}</div>
                                            </div>
                                        @endif
                                        <div class="form-group form-md-line-input">
                                            <a class="btn btn-success" href="{{ route('overtime.edit', ['overtime' => $overtime->id]) }}">Edit</a>
                                            @if($overtime->id != 1 && $overtime->id != 2 && $overtime->id != 3)
                                                <a href="{{ route('overtime.destroy', $overtime->id) }}" class="btn btn-danger" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a>
                                            @endif
                                            <a class="btn btn-warning" href="{{ URL::previous() }}">Back</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($overtime->type == 'slab')
                                            <h4 class="caption-subject bold uppercase">Slabs</h4>
                                            <table class="table table-stripped table-hover table-responsive">
                                                <thead>
                                                <tr class="row">
                                                    <th class="col-sm-1 text-right">#</th>
                                                    <th class="col-sm-4 text-right">From</th>
                                                    <th class="col-sm-4 text-right">To</th>
                                                    <th class="col-sm-3 text-right">Rate</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($slabs as $slab)
                                                    <tr class="row">
                                                        <td class="col-sm-1 text-right">{{ $slab->slab_number }}</td>
                                                        @if($slab->max_time == 0)
                                                            <td class="col-sm-8 text-right" colspan="2">{{ $slab->max_time == 0 ? 'Above ' . number_format($slab->min_time, 2) :  number_format($slab->min_time, 2) }}</td>
                                                        @else
                                                            <td class="col-sm-4 text-right">{{  number_format($slab->min_time, 2) }}</td>
                                                            <td class="col-sm-4 text-right">{{ number_format($slab->max_time, 2) }}</td>
                                                        @endif
                                                        <td class="col-sm-3 text-right">{{ number_format($slab->rate, 2) }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="payments">
                                <h3>Employee overtimeShows</h3>
                                <hr>
                                <table class="table table-striped table-hover table-responsive dataTable">
                                    <thead>
                                    <tr>
                                        <th>For Month</th>
                                        <th>Payroll Number</th>
                                        <th>Employee Name</th>
                                        <th>overtimeShow Number</th>
                                        <th>Amount Deducted</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="employees">
                                <span class="pull-right">

                                </span>
                                <h3>Employees with {{ $overtime->name }}</h3>
                                <hr>
                                <table class="table table-striped table-hover table-responsive dataTable" id="allowances_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Payroll Number
                                        </th>
                                        <th>
                                            Identification
                                        </th>
                                        <th>
                                            Full Names
                                        </th>
                                        <th>
                                            Gender
                                        </th>
                                        <th>
                                            Mobile Phone
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        <th>
                                            Edit
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection